/**
 * 
 */
package compiler1;

/**
 * @author Mahsa Riyahi
 *
 */

import java.util.Scanner; 
import java.util.ArrayList;
import java.util.*;  
import java.io.*;



public class parser1 {


	

static int i=0;
static int n=0;
static int p=0;
static String id = "id";
	public static void token(String s) {	
		
		 StringTokenizer st = new StringTokenizer(s); 
	     while (st.hasMoreTokens()) {
	     String data = st.nextToken(); 
	     
	     if(data.equals("ERROR")){
	     	n++;
		}
	     else if(data.equals("#")){
	    	 parser(data);
		}
	     else if(data.equals("\\")) {
	    	 i++; 
	     }
	     else if(data.equals("ID")) {
	    	 p++; 
	     }
	     else if(i==1 && p==0) {
	    	//System.out.println(data);
	    	 parser(data);
	    	 i--;
	    	 }
	     else if(i==1 && p==1) {
	    	 //System.out.println(data);
	    	 parser(id);
	    	 i--;
	    	 p--;
	    	 }
	     
	     }    
	}
	static int q=0;
	public static void main(String[] args) {
		
		
		try  
		 {  
		  
		 FileInputStream fis=new FileInputStream("scanneroutput.txt");       
		 Scanner sc=new Scanner(fis);     
		 while(sc.hasNextLine())  { 
			 if(q==0) {
				 String str = sc.nextLine();
				 token(str);
			 }
			 else {
				 break;
			 }
		 }
		 sc.close();    
		 }  
		 catch(IOException e)  
		 {  
		 e.printStackTrace();  
		 }  
		  
		


}
	
	static  ArrayList<String> push= new ArrayList<String>();
	static Stack<String> stk = new Stack<>();
	static int b=0;
	static int t=0;

	

	public static void parser(String a) {
		
	
	
	
		
		ArrayList<String> top= new ArrayList<String>();
		top.add(" ");
		top.add("S'");
		top.add("S");
		top.add("S1");
		top.add("S4");
		top.add("Q");
		top.add("S2");
		top.add("S3");
		top.add("B");
		top.add("E");
		top.add("A");
		top.add("N");
		top.add("A1");
		top.add("D");
		top.add("I");
		top.add("C");
		top.add("OP1");
		top.add("F");
		top.add("S5");
		top.add("W");
		top.add("R");
		top.add("S6");
		top.add("K");
		top.add("L");
		top.add("NV");
		top.add("VNUM1");
		top.add("S8");
		top.add("CH");
		top.add("P");
		top.add("S7");
		top.add("NUM");
		top.add("NUM1");
		top.add("Tab");
		top.add("Q1");
	   
		
		ArrayList<String> token= new ArrayList<String>();
		token.add(" ");
		token.add("0"); 
		token.add("9"); 
		token.add("8");
		token.add("7");
		token.add("6"); 
		token.add("5");
		token.add("4");
		token.add("3");
		token.add("2");
		token.add("1");
		token.add("cmr");
		token.add("cml");
		token.add("cm");
		token.add("\"");
		token.add(":=");
		token.add("character");
		token.add(",");
		token.add("word");
		token.add("real");
		token.add("integer");
		token.add("array");
		token.add(")");
		token.add("(");
		token.add("|");
		token.add("&");
		token.add("=");
		token.add("<");
		token.add(">");
		token.add("!=");
		token.add("<=");
		token.add(">=");
		token.add("id");
		token.add(".");
		token.add("^");
		token.add("*");
		token.add("//");
		token.add("/");
		token.add("-");
		token.add("-");
		token.add("+");
		token.add("tab");
		token.add("until");
		token.add("condition");
		token.add("%");
		token.add("#");
		
		
		String[][] ptable = new String[34][46] ;
		ptable[8][1] = "A E";
		ptable[8][2] = "A E";
		ptable[8][3] = "A E";
		ptable[8][4] = "A E";
		ptable[8][5] = "A E";
		ptable[8][6] = "A E";
		ptable[8][7] = "A E";
		ptable[8][8] = "A E";
		ptable[8][9] = "A E";
		ptable[8][10] = "A E";
		ptable[10][1] = "N A1";
		ptable[10][2] = "N A1";
		ptable[10][3] = "N A1";
		ptable[10][4] = "N A1";
		ptable[10][5] = "N A1";
		ptable[10][6] = "N A1";
		ptable[10][7] = "N A1";
		ptable[10][8] = "N A1";
		ptable[10][9] = "N A1";
		ptable[10][10] = "N A1";
		ptable[11][1] = "D NUM1";
		ptable[11][2] = "D NUM1";
		ptable[11][3] = "D NUM1";
		ptable[11][4] = "D NUM1";
		ptable[11][5] = "D NUM1";
		ptable[11][6] = "D NUM1";
		ptable[11][7] = "D NUM1";
		ptable[11][8] = "D NUM1";
		ptable[11][9] = "D NUM1";
		ptable[11][10] = "D NUM1";
		ptable[13][1] = "NUM";
		ptable[13][2] = "NUM";
		ptable[13][3] = "NUM";
		ptable[13][4] = "NUM";
		ptable[13][5] = "NUM";
		ptable[13][6] = "NUM";
		ptable[13][7] = "NUM";
		ptable[13][8] = "NUM";
		ptable[13][9] = "NUM";
		ptable[13][10] = "NUM";
		ptable[15][1] = "B OP1 B F";
		ptable[15][2] = "B OP1 B F";
		ptable[15][3] = "B OP1 B F";
		ptable[15][4] = "B OP1 B F";
		ptable[15][5] = "B OP1 B F";
		ptable[15][6] = "B OP1 B F";
		ptable[15][7] = "B OP1 B F";
		ptable[15][8] = "B OP1 B F";
		ptable[15][9] = "B OP1 B F";
		ptable[15][10] = "B OP1 B F";
		ptable[24][1] = "NUM1 VNUM1";
		ptable[24][2] = "NUM1 VNUM1";
		ptable[24][3] = "NUM1 VNUM1";
		ptable[24][4] = "NUM1 VNUM1";
		ptable[24][5] = "NUM1 VNUM1";
		ptable[24][6] = "NUM1 VNUM1";
		ptable[24][7] = "NUM1 VNUM1";
		ptable[24][8] = "NUM1 VNUM1";
		ptable[24][9] = "NUM1 VNUM1";
		ptable[24][10] = "NUM1 VNUM1";
		ptable[30][1] = "0";
		ptable[30][2] = "9";
		ptable[30][3] = "8";
		ptable[30][4] = "7";
		ptable[30][5] = "6";
		ptable[30][6] = "5";
		ptable[30][7] = "4";
		ptable[30][8] = "3";
		ptable[30][9] = "2";
		ptable[30][10] = "1";
		ptable[31][1] = "NUM NUM1";
		ptable[31][2] = "NUM NUM1";
		ptable[31][3] = "NUM NUM1";
		ptable[31][4] = "NUM NUM1";
		ptable[31][5] = "NUM NUM1";
		ptable[31][6] = "NUM NUM1";
		ptable[31][7] = "NUM NUM1";
		ptable[31][8] = "NUM NUM1";
		ptable[31][9] = "NUM NUM1";
		ptable[31][10] = "NUM NUM1";
		ptable[1][12] = "S #";
		ptable[2][12] = "S7 S";
		ptable[1][13] = "S #";
		ptable[2][13] = "S7 S";
		ptable[1][16] = "S #";
		ptable[2][16] = "S8 % S";
		ptable[1][18] = "S #";
		ptable[2][18] = "S5 % S";
		ptable[1][19] = "S #"; 
		ptable[2][19] = "S1 % S";
		ptable[3][19] = "real S4";
		ptable[1][20] = "S #";
		ptable[2][20] = "S1 % S";
		ptable[3][20] = "integer S4";
		ptable[1][21] = "S #";
		ptable[2][21] = "S6 % S";
		ptable[1][32] = "S #";  
		ptable[2][32] = "S4 % S";
		ptable[1][42] = "S #";   
		ptable[2][42] = "S3 S";
		ptable[1][43] = "S #";   
		ptable[2][43] = "S2 S";
		ptable[1][45] = "S #";
		ptable[2][45] = "epsilon";
		ptable[5][15] = ":= B";
		ptable[5][17] = ", NV";
		ptable[4][32] = "I Q";
		ptable[5][44] = "epsilon";
		ptable[6][43] = "condition ( C ) Tab";
		ptable[7][42] = "until ( C ) Tab";
		ptable[8][23] = "A E";
		ptable[8][32] = "A E";
		ptable[8][38] = "A E";
		ptable[9][22] = "epsilon";
		ptable[9][23] = "epsilon";
		ptable[9][24] = "epsilon";
		ptable[9][25] = "epsilon";
		ptable[9][26] = "epsilon";
		ptable[9][27] = "epsilon";
		ptable[9][28] = "epsilon";
		ptable[9][29] = "epsilon";
		ptable[9][30] = "epsilon";
		ptable[9][31] = "epsilon";
		ptable[9][33] = ". A E";
		ptable[9][39] = "- A E";
		ptable[9][40] = "+ A E";
		ptable[9][44] = "epsilon";
		ptable[10][23] = "N A1";
		ptable[10][32] = "N A1";
		ptable[10][38] = "N A1";
		ptable[11][23] = "D NUM1";
		ptable[11][32] = "D NUM1";
		ptable[11][38] = "- D NUM1";
		ptable[12][22] = "epsilon";
		ptable[12][23] = "epsilon";
		ptable[12][24] = "epsilon";
		ptable[12][25] = "epsilon";
		ptable[12][26] = "epsilon";
		ptable[12][27] = "epsilon";
		ptable[12][28] = "epsilon";
		ptable[12][29] = "epsilon";
		ptable[12][30] = "epsilon";
		ptable[12][31] = "epsilon";
		ptable[12][33] = "epsilon";
		ptable[12][34] = "^ D A1";
		ptable[12][35] = "* D A1";
		ptable[12][36] = "// D A1";
		ptable[12][37] = "/ D A1";
		ptable[12][39] = "epsilon";
		ptable[12][40] = "epsilon";
		ptable[12][44] = "epsilon";
		ptable[13][23] = "( B )";
		ptable[13][32] = "I";
		ptable[14][32] = "id";
		ptable[15][23] = "B OP1 B F";
		ptable[15][32] = "B OP1 B F";
		ptable[15][38] = "B OP1 B F";
		ptable[16][26] = "=";
		ptable[16][27] = "<";
		ptable[16][28] = ">";
		ptable[16][29] = "!=";
		ptable[16][30] = "<=";
		ptable[16][31] = ">=";
		ptable[17][22] = "epsilon";
		ptable[17][23] = "( C )";
		ptable[17][24] = "| C"; 
		ptable[17][25] = "& C";
		ptable[18][18] = "word W";
		ptable[20][15] = ":= \" \"";
		ptable[19][32] = "I R";
		ptable[21][21] = "array K";
		ptable[22][18] = "word L"; 
		ptable[22][19] = "real L";
		ptable[22][20] = "integer L";
		ptable[23][32] = "I Q1";
		ptable[24][17] = "NUM1 VNUM1";
		ptable[24][44] = "NUM1 VNUM1";
		ptable[25][17] = ", NUM1";
		ptable[25][44] = "epsilon";
		ptable[26][16] = "character CH";
		ptable[28][15] = ":= \" \"";
		ptable[27][32] = "I P";
		ptable[29][12] = "cml cmr";
		ptable[29][13] = "cm";
		ptable[31][17] = "epsilon";
		ptable[31][22] = "epsilon";
		ptable[31][23] = "epsilon";
		ptable[31][24] = "epsilon";
		ptable[31][25] = "epsilon";
		ptable[31][26] = "epsilon";
		ptable[31][27] = "epsilon";
		ptable[31][28] = "epsilon";
		ptable[31][29] = "epsilon";
		ptable[31][30] = "epsilon";
		ptable[31][31] = "epsilon";
		ptable[31][33] = "epsilon";
		ptable[31][34] = "epsilon";
		ptable[31][35] = "epsilon";
		ptable[31][36] = "epsilon";
		ptable[31][37] = "epsilon";
		ptable[31][39] = "epsilon";
		ptable[31][40] = "epsilon";
		ptable[31][44] = "epsilon";
		ptable[32][41] = "tab";
		ptable[33][17] = ", NV";
		
		

		if(b==0) {
			stk.push("S'");
			b++;	
		}
		if(!token.contains(a)) {
			System.out.println("NO");
			q++;
			t++;
		}
		
		while(!stk.empty() && t==0) {
			int e=0;
			push.clear();
				if(a.equals(stk.peek()) && !a.equals("#")) {
					stk.pop();
					//System.out.println(stk);
					break;	     
	            }
				else if(a.equals(stk.peek()) && a.equals("#")) {
					System.out.println("YES");
					break;
				}
				else if(top.contains(stk.peek()) && token.contains(a)) {
					
					int i= top.indexOf(stk.peek());
					int j =token.indexOf(a);
		             if(ptable[i][j] == null) {
							System.out.println("NO");
							q++;
							break; 
		             }
		            else if(!a.equals(stk.peek())){
						stk.pop();
						StringTokenizer st = new StringTokenizer(ptable[i][j]); 
						while (st.hasMoreTokens()) {
							String data = st.nextToken(); 
							if(data.equals("epsilon")) {
								e++;
							}
							else {
								push.add(data);
							}
						}
						if(e==0) {
							for(int p=push.size()-1 ; p>0 ; p--) {
								stk.push(push.get(p));}
								stk.push(push.get(0));	
								//System.out.println(stk);
						}
					}
				}

		 }
					
	}
	
}
	


	
	
	
	
	



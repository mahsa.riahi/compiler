/**
 * 
 */
package compiler1;

/**
 * @author Mahsa Riyahi
 *
 */


import java.util.Scanner; 
import java.util.ArrayList;
import java.util.*;  
import java.io.*;


public class scanner1 {
	

public static void token(String s) {	
	 
	 StringTokenizer st = new StringTokenizer(s); 
     while (st.hasMoreTokens()) {
         String data = st.nextToken(); 
         scanner(data);
	}
}


static int i;
public static void main(String[] args)  throws FileNotFoundException  { 
	
	
    PrintStream o = new PrintStream(new File("scanneroutput.txt")); 
    System.setOut(o); 
 

		
	try  
		 {  
		  
		 FileInputStream fis=new FileInputStream("TestMahsaRiyahi.txt");       
		 Scanner sc=new Scanner(fis);     
		 while(sc.hasNextLine())  { 
		 String str = sc.nextLine();
			
			if(str.length()>=2) {
				char c1=str.charAt(0);
				char c2=str.charAt(1);
				char cl=str.charAt((str.length())-1);
				char cll=str.charAt((str.length())-2);
				int asc=c1;
				int asc1 = c2;
	
				if(asc==9 && i==0) {
					if(asc1==9) {
						System.out.println("( DLM_OP \\ " + " tab )" );
						System.out.println(" ERROR ");
						token(str);
					}
					else {
					System.out.println("( DLM_OP \\ " + " tab )" );
					token(str);
				}
				}
				else if(c1 != '|' && c2 != '|' && i==0) {
					token(str);
				}	
				else if(c1 !='|' && c2 !='.' && i==0) {
					token(str);	
				}
				
				else if(c1 == '|' && c2 == '|' && i==0) {
					System.out.println("( DLM_OP \\ " + " cm )" );
				}
				else if(c1 =='|' && c2 =='.') {
					i=1;
					System.out.println("( DLM_OP \\ " + " cml )" );	
				}
				else if(c1 !='|' && c2 !='.' && i==1  && cl != '|' && cll != '.') {
					i=1;
			    }
				else if(cl == '|' && cll == '.' && i==1) {
					i=0;
					System.out.println("( DLM_OP \\ " + " cmr )" );
				
				}
				else if(asc==9 && i==1) {
					i=0;
					System.out.println("( DLM_OP \\ " + " cmr )" );
					
					
				}
				
			}
			else if(str.length()<2) {
				token(str);
		 } }
		 sc.close();    
		 }  
		 catch(IOException e)  
		 {  
		 e.printStackTrace();  
		 }  
	
	
	
	System.out.println("#");


}



 
 
 
     public static ArrayList<String> ID= new ArrayList<String>();
	 static ArrayList<String> ST= new ArrayList<String>();
	 static int b=0;
	
	 
	 public static void scanner(String a) {
		 
		 String[][] RS = new String[7][2] ;
		 RS[0][0] = "integer";  RS[0][1] = "INT";
		 RS[1][0] = "real";  RS[1][1] = "REAL";
		 RS[2][0] = "word"; RS[2][1] = "WORD";
		 RS[3][0] = "character"; RS[3][1] = "CHAR";
		 RS[4][0] = "array";  RS[4][1] = "ARR";
		 RS[5][0] = "condition"; RS[5][1] = "COND";
		 RS[6][0] = "until"; RS[6][1] = "UNTIL";
		 
		 
		 String[][] DLM_OP = new String[][] {
			 {"%" , "SEM"},
			 {":=" , "ASG"},
			 {"=" , "EQ"},
			 {">" , "G"},
			 {"<" , "L"},
			 {">=" , "GE"},
			 {"<=" , "LE"},
			 {"!=" , "NEQ"},
			 {")" , "PR"},
			 {"(" , "PL"},
			 {"+" , "ADD"},
			 {"-" , "SUB"},
			 {"*" , "MUL"},
			 {"^" , " PW"},
			 {"&" , "AND"},
			 {"|" , "OR"},
			 {"/" , "SL"},
			 {"//" , "MOD"},
			 {"," , "COMMA"},
			 {"\"" , "QT"},
			 	
		 };
	
		char ch=a.charAt(0);
		int asc = ch;
		boolean rs=false;
		boolean dl=false;
    	int length=a.length();
    	
	    for(int k=0; k<=19 ; k++) {
			 if (a.equals(DLM_OP[k][0])) {
				 System.out.println("( DLM_OP \\ " + DLM_OP[k][0] + " )");
				 dl=true;}
	    }	
		
	    for(int i=0; i<=6 ; i++) {
			 if (a.equals(RS[i][0])) {
				 System.out.println("( RS \\ " + RS[i][0] + " )");
				 rs=true;}
	    }
	    if(asc==34) {
	    	b++;
	    }
	    else if (!ID.contains(a) && rs==false && dl==false) {
	    	if(b==0) {
	    		if ((65<=asc && asc<=90) || (97<=asc && asc<=122)) {
	    			ID.add(a);
	    			System.out.println("( ID \\ " + a + " )");
	    			
	         }
	    		else  if(asc==45) {
	    			if(checkInteger(a)) {
	    				getChar1(a);
	    				//System.out.println("( -integer , " + a + " )");	
			    }
	    			else if(checkFloat(a) && length>=2) {
                	
	    				String r2="" + a.charAt(1);
	    				String r3="" + a.charAt(length-1);
	    				if(!r2.equals(".") && !r3.equals(".")){
	    					getChar1(a);
	    					//System.out.println("( -real , " + a + " )");
						}
	    				else{
	    					System.out.println("ERROR");
                 	 }
                }	 
			    	
                else {
			    	System.out.println("ERROR");
			    	
			    }}
	    	else  if(48<=asc && asc<=57) {
			    if(checkInteger(a)) {
			    	getChar(a);
			    	//System.out.println("( integer , " + a + " )");
			 }
			    else if(checkFloat(a)) {
			    	
			    	String r1="" + a.charAt(0);
			    	String r="" + a.charAt(length-1);
					
			    	if(r.equals(".")) {
						System.out.println("error");
						}
			    	else if(r1.equals(".")){
						System.out.println("error");	
						}
			    	else if (checkFloat(a)){
			    		getChar2(a);
						//System.out.println("( real , " + a + " )");
						}
					else {
				    	System.out.println("ERROR");
				            }
			     }
			    else {
			    	System.out.println("ERROR");
			    	
			    }
	    	}
	    	   else {
	   	    	System.out.println("ERROR");
	   		            }		
	    }
	    
	    	else if(b==1) {
	    		b=-1;
	    			if(length==1) {
	    				ST.add(a);	
	    		}
	    			else if(length>=2) {
	    				ST.add(a);
	    				
	    			}
	    		
	    	}
	    }
	    
	    else if (ID.contains(a)){
	    	 if(b==0) {
			//	 int position = ID.indexOf(a)+1;
				 System.out.println("( ID \\ " + a + " )");
			}
	    	 else if(b==1) {
	    		 b=-1;
	    			if(length==1) {
	    				ST.add(a);	
	    		}
	    			else if(length>=2) {
	    				ST.add(a);	
	    				
	    			}
	    	 }
	    		 
	     }
	    
	    
	
	}
     
	 
	 
	 public static void getChar(String str) {
		 char s;
		 for(int index=0 ; index<str.length() ; index++) {
			 s=str.charAt(index);
			 System.out.println("( NUM \\ " + s + " )"); 
		 }
	 }
	 
	 public static void getChar1(String str) {
		 char s;
		 char s1;
		 s1=str.charAt(0);
		 System.out.println("( MANFI \\ " + s1 + " )");
		 for(int index=1 ; index<str.length() ; index++) {
			 s=str.charAt(index);
			 System.out.println("( NUM \\ " + s + " )"); 
		 }
	 }
	 
	 public static void getChar2(String str) {
		 char s;
		 for(int index=0 ; index<str.length() ; index++) {
			 s=str.charAt(index);
			 if(s=='.') {
				 System.out.println("( DOT \\ " + s + " )");  
			 }
			 else {
			 System.out.println("( NUM \\ " + s + " )"); 
		 }
	 }
	 
	 }
	 
	 
	 public static boolean checkInteger(String s) {
	    	
	    	try 
	    	{ 
         
         Integer.parseInt(s); 
         return true; 
     }  
     catch (NumberFormatException e)  
     { 
         return false; 
     }
	
	    }
	 
	 
	 public static boolean checkFloat(String s) {
		 try
	        { 
	            
	            Float.parseFloat(s); 
	            return true; 
	        }  
	        catch (NumberFormatException e) 
	        { 
	            return false; 
	        }
	 
	 }

	 
 
		
	 }
	
